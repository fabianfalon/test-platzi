# Generated by Django 2.1 on 2018-08-23 20:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0003_remove_user_plan'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='plan',
            field=models.IntegerField(choices=[(1, 'monthly'), (2, 'annual')], default=1),
        ),
    ]
