from django.db import models
from django.conf import settings

# Create your models here.
class Payment(models.Model):
    TYPE_PLAN = (
        (1, 'monthly'),
        (2, 'annual'),
    ) 
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        null=True, blank=True
    )
    description = models.TextField(blank=True)
    price = models.DecimalField(max_digits=64, decimal_places=2)
    plan = models.IntegerField(choices=TYPE_PLAN)
    applied_discount = models.IntegerField(default=0)
    currency = models.CharField(max_length=3, default=settings.CURRENCY)
    status = models.CharField(max_length=10, default='pending')
    extra_data = models.TextField(blank=True)
    paid_date = models.DateTimeField(auto_now=True)
    paid_until = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('price', '-paid_date')


    def save(self, *args, **kwargs):
        super(Subscription, self).save(*args, **kwargs)


class Audit(models.Model):

    subscription_data = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        null=True, blank=True
    )