from .base import *
import sys
import os
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'OPTIONS': {
            'sql_mode': 'traditional',
        },
        'HOST': os.environ['MYSQL_HOST'],
        'NAME': os.environ['MYSQL_DATABASE'],
        'USER': os.environ['MYSQL_USER'],
        'PASSWORD': os.environ['MYSQL_USER'],
        'PORT': 3306
    }
}

STRIPE_API_KEY = 'pk_test_mj0cCg5i94B2AmwPmIzdJuJ8'

CURRENCY = 'USD'

PLANS = {
    '1': {
        'price': 29,
        'name': 'monthly',
        'description': 'Pago mensual',
    },
    '2': {
        'price': 299,
        'name': 'annual',
        'description': 'Pago anual',
    },
}