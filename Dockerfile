FROM python:3.6

ENV PYTHONUNBUFFERED 1

RUN apt-get clean all && apt-get update && apt-get upgrade -y && \
    apt-get install -y python3-dev build-essential curl gettext libjpeg-dev default-libmysqlclient-dev && \
    apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY ./requirements /requirements
RUN pip install -r /requirements/local.txt

WORKDIR /app